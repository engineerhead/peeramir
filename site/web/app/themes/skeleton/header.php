<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <title><?php wp_title(''); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(isset($class) ? $class : ''); ?>>
	<?php get_template_part("templates/header", "banner"); ?>

	<?php get_template_part("templates/logo", "banner"); ?>

	<?php get_template_part("templates/nav", "main"); ?>
    <div id="main-container" class="container">
